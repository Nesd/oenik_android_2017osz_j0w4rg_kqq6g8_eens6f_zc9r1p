package utility;

import android.graphics.Bitmap;
import android.graphics.RectF;

/**
 * Created by Nesd on 2017.11.19..
 * Ezt öröklik a játék objektumai
 */

public abstract class GameExtent {
    //Movement states
    public final int STOPPED = 0;
    public final int LEFT = 1;
    public final int RIGHT = 2;
    public final static int DEAD = 3;
    public final static int NORMAL = 4;

    //Current state of objects
    protected int state = STOPPED;
    public int getState(){ return state; }
    public void setState(int state) { this.state = state; }

    //For collision detection
    protected RectF rectF;
    public RectF getRect(){
        return rectF;
    }

    //Coordinates of the object
    protected float posX;
    public float getPosX() { return posX; }
    protected float posY;
    public float getPosY() { return posY; }

    //Space of the object
    protected float width;
    protected float height;

    //Speed of the object
    protected float speed;
    public void setSpeed(int speed) { this.speed = speed; }

    //Image of the object
    protected Bitmap bitmap;
    public Bitmap getBitmap(){ return bitmap; }

    protected int screenX;
    protected int screenY;
}
