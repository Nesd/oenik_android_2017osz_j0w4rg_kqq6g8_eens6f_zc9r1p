package utility;

import java.util.Random;

/**
 * Created by Nesd on 2017.11.19..
 */

public abstract class Const {
    public static final int MAX_FPS = 30;
    public static final int REVIVE_RATIO = 2;

    public static Random RND = new Random();

    public static final int TEXT_TINY = 14;
    public static final int TEXT_TITLE = 18;

    public static final String KEY_PREF_VIBRATE = "pref_vibrate";
    public static final String KEY_PREF_VOLUME = "pref_volume";
    public static final String KEY_PREF_BUTTOB_BACK = "pref_button_back";
    public static final String KEY_PREF_USERNAME = "pref_username";
    public static final String KEY_PREF_AVATAR = "pref_avatar";
    public static final String KEY_PREF_CHANGE = "pref_change";
}
