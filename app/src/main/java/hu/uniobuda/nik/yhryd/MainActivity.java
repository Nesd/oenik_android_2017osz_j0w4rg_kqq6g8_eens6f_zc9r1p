package hu.uniobuda.nik.yhryd;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import hu.uniobuda.nik.yhryd.LogIn.FirstLogInActivity;
import hu.uniobuda.nik.yhryd.ScoreBoard.ScoreBoardActivity;
import hu.uniobuda.nik.yhryd.ScoreBoard.ScoreDatabase;
import hu.uniobuda.nik.yhryd.ScoreBoard.UserScore;
import utility.Const;

public class MainActivity extends AppCompatActivity {

    TextView title;
    ArrayList<Button> buttons;
    DisplayMetrics dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if(!sharedPreferences.getAll().containsKey(Const.KEY_PREF_AVATAR)) {
            Intent intent = new Intent(getBaseContext(), FirstLogInActivity.class);
            startActivity(intent);
        }


        setMenuButtons();
        //startMusicService();

        dm = getResources().getDisplayMetrics();

        title = (TextView) findViewById(R.id.title);
        buttons =  new ArrayList<Button>(){};
        buttons.add((Button) findViewById(R.id.button_play));
        buttons.add((Button) findViewById(R.id.button_tutorial));
        buttons.add((Button) findViewById(R.id.button_scoreboard));
        buttons.add((Button) findViewById(R.id.button_options));
        buttons.add((Button) findViewById(R.id.button_exit));

        float titleTextSize = dm.widthPixels / 25;
        float buttonTextSize = dm.widthPixels / 75;

        title.setTextSize(titleTextSize);
        //buttons.forEach(item -> item.setTextSize(buttonTextSize));
        for (Button item: buttons){
            item.setTextSize(buttonTextSize);
        }

        int buttonWidth = dm.widthPixels / 3;
        for (Button item: buttons){
            item.setWidth(buttonWidth);
        }

    }


    public void startMusicService() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPref.getBoolean(Const.KEY_PREF_VOLUME, true)) {
            startService(new Intent(getApplication(), MusicService.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        startMusicService();
    }

    private void setMenuButtons() {

        Button button = findViewById(R.id.button_play);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gameActivity = new Intent(getBaseContext(), GameActivity.class);
                startActivity(gameActivity);
            }
        });

        button = findViewById(R.id.button_scoreboard);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scoreboardActivity = new Intent(getBaseContext(), ScoreBoardActivity.class);
                startActivity(scoreboardActivity);
            }
        });

        button = findViewById(R.id.button_options);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent optionsActivity = new Intent(getBaseContext(), SettingsActivity.class);
                startActivity(optionsActivity);
            }
        });

        button = findViewById(R.id.button_tutorial);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tutorial = new Intent(getBaseContext(), Tutorial.class);
                startActivity(tutorial);
            }
        });

        button = findViewById(R.id.button_exit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MusicService.getMusicService() != null)
                    MusicService.getMusicService().stopSelf();
                finish();
                System.exit(0);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (MusicService.getMusicService() != null)
            MusicService.getMusicService().stopSelf();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (MusicService.getMusicService() != null)
            MusicService.getMusicService().stopSelf();
    }
}
