package hu.uniobuda.nik.yhryd;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;

import java.util.Map;
import java.util.Set;

import hu.uniobuda.nik.yhryd.LogIn.FirstLogInActivity;
import utility.Const;

/**
 * Created by galba on 2017. 11. 14..
 */

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_settings);

        // Default értékek beállítása
        PreferenceManager.setDefaultValues(getActivity(), R.xml.pref_settings, true);

        // Back gomb beállítása
        Preference button = getPreferenceScreen().findPreference(Const.KEY_PREF_BUTTOB_BACK);
        button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                getActivity().finish();
                return true;
            }
        });

        button = getPreferenceScreen().findPreference(Const.KEY_PREF_CHANGE);
        button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(getActivity(), FirstLogInActivity.class);
                startActivity(intent);
                return true;
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        // Vibrate vizsgáalata
        if (key.equals(Const.KEY_PREF_VIBRATE)) {
            Preference vibratePreference = findPreference(key);
            if (sharedPreferences.getBoolean(key, true)){
                Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                if (vibrator !=null) vibrator.vibrate(500);
            }
        }

        //Hang Vizsgálata
        if (key.equals(Const.KEY_PREF_VOLUME)) {
            if(!sharedPreferences.getBoolean(key, true)){}
                MusicService.getMusicService().stopSelf();
        }

        // Username vizsgálata
        if(key.equals(Const.KEY_PREF_USERNAME)) {
            Preference usernamePreference = findPreference(key);
            if (usernamePreference instanceof EditTextPreference) {
                updateSummary((EditTextPreference) usernamePreference);
            }

        }
    }

    private void updateSummary(EditTextPreference preference) {
        preference.setSummary(preference.getText());
        ((BaseAdapter) getPreferenceScreen().getRootAdapter()).notifyDataSetChanged();
    }
}
