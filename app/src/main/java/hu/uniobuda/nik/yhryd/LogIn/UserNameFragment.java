package hu.uniobuda.nik.yhryd.LogIn;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import junit.framework.Assert;

import hu.uniobuda.nik.yhryd.R;
import utility.Const;

/**
 * Created by galba on 2017. 12. 10..
 */

public class UserNameFragment extends android.app.Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_name, container, false);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button button = (Button) getActivity().findViewById(R.id.button_first_step);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) getActivity().findViewById(R.id.edittext_first_step);
                String userName = editText.getText().toString();

                if(userName.matches("")) {

                    Toast.makeText(
                            getActivity(),
                            "Nem hagyhatod üresen a mezőt",
                            Toast.LENGTH_SHORT
                    ).show();

                }
                else {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Const.KEY_PREF_USERNAME, userName);
                    editor.commit();



                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, new AvatarSelectorFragment()).commit();
                }
            }
        });
    }
}
