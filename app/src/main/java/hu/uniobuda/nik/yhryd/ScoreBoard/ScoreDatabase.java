package hu.uniobuda.nik.yhryd.ScoreBoard;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by galba on 2017. 12. 09..
 */

@Database(entities = {UserScore.class}, version = 4)
public abstract class ScoreDatabase extends RoomDatabase {

    private static ScoreDatabase scoreDatabase;

    public abstract ScoreDao scoreDao();

    protected ScoreDatabase(){}

    public static synchronized ScoreDatabase getInstance(Context context){
        if(scoreDatabase == null){
            final Builder<ScoreDatabase> scoreDatabaseBuilder = Room.databaseBuilder(context.getApplicationContext(), ScoreDatabase.class, "database-score");
            scoreDatabaseBuilder.fallbackToDestructiveMigration();
            scoreDatabaseBuilder.allowMainThreadQueries();
            scoreDatabase = scoreDatabaseBuilder.build();
        }

        return scoreDatabase;
    }
}
