package hu.uniobuda.nik.yhryd;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;

import utility.Const;
import utility.GameExtent;

/**
 * Created by Nesd on 2017.11.19..
 */

public class GameButton extends GameExtent {
    //Image of the object
    private Bitmap bitmap2;
    public Bitmap getBitmap2(){ return bitmap2; }

    private Bitmap bitmapCross;
    public Bitmap getBitmapCross(){ return bitmapCross; }

    public GameButton(Context context, float width, float height, float posX, float posY) {
        this.posX = posX;
        this.posY = posY;

        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.btn_play);
        this.bitmap = Bitmap.createScaledBitmap(bitmap, (int) (width), (int) (height), false);

        this.bitmap2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.btn_pause);
        this.bitmap2 = Bitmap.createScaledBitmap(bitmap2, (int) (width), (int) (height), false);

        this.bitmapCross = BitmapFactory.decodeResource(context.getResources(), R.drawable.crosshair);
        this.bitmapCross = Bitmap.createScaledBitmap(bitmapCross, (int) (width), (int) (height), false);

        this.rectF = new RectF(posX, posY, posX + width, posY + height);
    }
}
