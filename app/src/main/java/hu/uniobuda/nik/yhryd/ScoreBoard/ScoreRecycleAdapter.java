package hu.uniobuda.nik.yhryd.ScoreBoard;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hu.uniobuda.nik.yhryd.R;

/**
 * Created by galba on 2017. 12. 09..
 */

public class ScoreRecycleAdapter extends RecyclerView.Adapter<ScoreRecycleAdapter.ScoreViewHolder> {

    private List<UserScore> userScores;

    private Context context;

    public List<UserScore> getUserScores() {
        return userScores;
    }

    public ScoreRecycleAdapter(List<UserScore> userScores, Context context) {
        this.userScores = userScores;
        this.context = context;
    }

    @Override
    public ScoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ScoreViewHolder(View.inflate(
                parent.getContext(),
                R.layout.listitem_score,
                null
        ));
    }

    @Override
    public void onBindViewHolder(ScoreViewHolder holder, int position) {
        UserScore myscore = userScores.get(position);
        holder.usernameTextview.setText(myscore.getUsername());
        holder.scoreTextview.setText(Integer.toString(myscore.getScore()));
        switch (myscore.getAvatar()){
            case "avatar1":
                holder.avatarImageView.setBackground(context.getResources().getDrawable(R.drawable.avatar1));
                break;
            case "avatar2":
                holder.avatarImageView.setBackground(context.getResources().getDrawable(R.drawable.avatar2));
                break;
            case "avatar3":
                holder.avatarImageView.setBackground(context.getResources().getDrawable(R.drawable.avatar3));
                break;
            case "avatar4":
                holder.avatarImageView.setBackground(context.getResources().getDrawable(R.drawable.avatar4));
                break;
        }

    }

    @Override
    public int getItemCount() {
        return userScores == null ? 0 : userScores.size();
    }

    public class ScoreViewHolder extends RecyclerView.ViewHolder {

        public TextView
                usernameTextview,
                scoreTextview;

        public ImageView avatarImageView;

        public ScoreViewHolder(View itemView) {
            super(itemView);
            usernameTextview = (TextView) itemView.findViewById(R.id.scoreboard_username);
            scoreTextview = (TextView) itemView.findViewById(R.id.scoreboard_score);
            avatarImageView =  itemView.findViewById(R.id.scoreboard_avatar);
        }
    }
}
