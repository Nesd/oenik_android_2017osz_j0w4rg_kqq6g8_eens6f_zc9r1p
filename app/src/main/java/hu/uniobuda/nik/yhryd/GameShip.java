package hu.uniobuda.nik.yhryd;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;

import utility.Const;
import utility.GameExtent;

/**
 * Created by Nesd on 2017.11.12..
 */

public class GameShip extends GameExtent {
    public float getWidth(){
        return this.width;
    }
    public GameShip(Context context, int screenX, int screenY) {
        this.screenX = screenX;

        this.width = screenX / 15;
        this.height = screenY / 15;

        this.posX = (screenX / 2) - (width / 2);
        this.posY = screenY - height - ((Const.TEXT_TINY + 4) * GameView.SP);

        this.speed = screenX;

        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.playership1);
        this.bitmap = Bitmap.createScaledBitmap(bitmap, (int) (width), (int) (height), false);

        this.rectF = new RectF();
    }

    public void update(long fps) {
        if (state == LEFT && posX >= 0) {
            posX -= speed / fps;
        }

        if (state == RIGHT && (posX + width) <= screenX) {
            posX += speed / fps;
        }

        // For collision detect
        rectF.left = posX;
        rectF.top = posY;
        rectF.right = posX + width;
        rectF.bottom = posY + height;
    }

    public boolean isCollided(GameAsteroid[] gameAsteroids)
    {
        for (int i = 0; i < gameAsteroids.length; i++) {
            if (gameAsteroids[i].getState() != DEAD && this.rectF.intersect(gameAsteroids[i].getRect())) {
                gameAsteroids[i].setState(DEAD);
                return true;
            }
        }
        return false;
    }
}
