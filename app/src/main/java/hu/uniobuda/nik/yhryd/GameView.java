package hu.uniobuda.nik.yhryd;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import hu.uniobuda.nik.yhryd.ScoreBoard.ScoreDatabase;
import hu.uniobuda.nik.yhryd.ScoreBoard.UserScore;
import utility.Const;

/**
 * Created by Nesd on 2017.11.12..
 */

public class GameView extends SurfaceView implements Runnable {
    private int bulletCounter;
    private long startFrameTime;
    private long lastBullet;
    private float pitchCounter = 0;
    private Context context;
    //unit
    public static float DP;
    public static float SP;

    //A kijelzo merete pixelben
    private int screenX;
    private int screenY;

    //Azert volatile, mert nem csak a thread-en belul fogjuk hasznalni.
    //Kezdetben szünetel a jatek
    private volatile boolean playing;
    private boolean paused = true;

    //Ezek az ertekek lathatoak lesznek a jatek ideje alatt.
    private int score = 0;
    private int lives = 5;
    private int fuel = 200;
    private int ammo = 50;

    //Paint -> Betumeret, stilus stb.
    //Canvas -> Erre rajzolunk
    private Canvas canvas;
    private Paint paint;

    //Lezarja a canvast, mielott rajzolunk ra (thread miatt szukseges)
    private SurfaceHolder surfaceHolder;

    //fps = frame per sec
    //Adott frame mennyi ideig volt lathato (fps-t ebbol fogjuk szamolni)
    private long fps;
    private long frameTime;

    private Thread gameThread = null;

    //Jatek objektumai alabb
    private GameShip gameShip;
    private GameButton pauseBtn;
    private GameButton crosshairBtn;
    private GameAsteroid[] gameAsteroids;
    private GameBullet[] gameBullets;

    //Szenzor osztaly
    OrientationData orientationData;
    private float roll;
    private float pitch;

    private Vibrator vb;
    SharedPreferences sharedPreferences;

    //Background
    private Bitmap background = BitmapFactory.decodeResource(getResources(), R.drawable.background);
    private Typeface tf = Typeface.createFromAsset(getResources().getAssets(), "fonts/kenvector_future.ttf");
    //private int lightPurple = context.getResources().getColor(R.color.lightPurple);

    public GameView(Context context, int x, int y) {
        super(context);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if(sharedPreferences.getBoolean(Const.KEY_PREF_VIBRATE, false))
            vb = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);

        DP = context.getResources().getDisplayMetrics().density;
        SP = context.getResources().getDisplayMetrics().scaledDensity;
        this.context = context;
        this.screenX = x;
        this.screenY = y;

        //init
        this.surfaceHolder = getHolder();
        this.paint = new Paint();

        this.orientationData = new OrientationData(context);
        this.orientationData.register();

        //Buttons init
        this.pauseBtn = new GameButton(context,
                30 * DP, 30 * DP,
                0, 5 * DP);

        //Corsshair init
        this.crosshairBtn = new GameButton(context,
                120*DP, 120*DP,
                screenX - 200*DP, screenY - 190*DP);

        initGameObjects();
    }

    private void initGameObjects() {
        //Ezt azert szerveztuk ki, mert majd hasznalni kell akkor is, ha ujrakezdjuk a jatekot stb.
        this.gameShip = new GameShip(context, screenX, screenY);
        this.gameAsteroids = new GameAsteroid[10];
        for (int i = 0; i < gameAsteroids.length; i++) {
            gameAsteroids[i] = new GameAsteroid(context, screenX, screenY, Const.RND.nextInt(screenX));
        }

        this.gameBullets = new GameBullet[30];
        for (int i = 0; i < gameBullets.length; i++) {
            gameBullets[i] = new GameBullet(context, screenX, screenY);
        }
        this.orientationData.newGame();

    }

    @Override
    public void run() {
        while (playing) {
            //Adott frame lettrejottenek idopontja
            startFrameTime = System.currentTimeMillis();

            //Csak akkor frissitjuk a frame-et, ha nem szunetel a jatek
            if (!paused) {
                update();
            }
            draw();
            frameTime = System.currentTimeMillis() - startFrameTime;

            if (frameTime >= 1) {
                //fps = frame per sec, 1000ms = 1 sec
                fps = 1000 / frameTime;
            }
        }
    }

    private void update() {
        //Itt tortenik minden objektum poziciojanak frissitese, utkozesek vizsgalata stb.

        boolean gameOver = false;

        //Player's ship
        if (orientationData.getOrientation() != null && orientationData.getStartOrientation() != null) {

            pitch = orientationData.getOrientation()[1] - orientationData.getStartOrientation()[1];
            roll = orientationData.getOrientation()[2] - orientationData.getStartOrientation()[2];
            if (fuel > 0) {
                if (Math.abs(pitch) > 0.1f) {
                    pitchCounter += Math.abs(pitch);
                    if (pitchCounter > 10) {
                        fuel--;
                        pitchCounter = 0;
                    }
                }
                if (pitch > 0.1f) {
                    gameShip.setState(gameShip.LEFT);
                    gameShip.setSpeed((int) (screenX * pitch));
                } else if (pitch < -0.1f) {
                    gameShip.setState(gameShip.RIGHT);
                    gameShip.setSpeed((int) (screenX * -pitch));
                } else
                    gameShip.setState(gameShip.STOPPED);
            } else
                gameShip.setState(gameShip.STOPPED);

            gameShip.update(fps);
        }


        for (int i = 0; i < gameAsteroids.length; i++) {
            if (gameAsteroids[i].getState() != GameAsteroid.DEAD) {
                gameAsteroids[i].update(fps);
                if (gameAsteroids[i].getState() == GameAsteroid.DEAD) {
                    score += 10;
                }
            } else {
                if (Const.RND.nextInt(100) <= Const.REVIVE_RATIO) {
                    gameAsteroids[i].Revive(gameShip.getPosX());
                }
            }
        }

        //Bullet
        for (int i = 0; i < gameBullets.length; i++) {
            if (gameBullets[i].getState() != GameBullet.DEAD) {
                gameBullets[i].update(fps);
                if (gameBullets[i].isCollided(gameAsteroids)) {
                    score += 100;
                }
            }
        }

        if (gameShip.isCollided(gameAsteroids)) {
            lives--;

            if(vb != null)
                vb.vibrate(500);

            //gameOver
            if (lives == 0) {
                UserScore userScore = new UserScore();
                userScore.setScore(score);
                userScore.setUsername(sharedPreferences.getString(Const.KEY_PREF_USERNAME, "DefaultPlayer"));
                userScore.setAvatar(sharedPreferences.getString(Const.KEY_PREF_AVATAR, "avatar1"));
                
                ScoreDatabase.getInstance(context).scoreDao().insertNewScore(userScore);
                ((GameActivity)context).finish();
            }
            paused = true;
            initGameObjects();
        }
    }

    private void draw() {
        //Gyakorlatilag itt tortenik a megjelenites

        if (surfaceHolder.getSurface().isValid()) {
            canvas = surfaceHolder.lockCanvas();

            //background image
            paint.setColor(Color.BLUE);
            paint.setAlpha(255);
            canvas.drawBitmap(background, 0, 0, paint);

            //Background color
            //canvas.drawColor(Color.argb(255, 50, 128, 130));

            //Brush color for drawing
            //paint.setColor(Color.argb(255, 50, 50, 50));

            canvas.drawBitmap(gameShip.getBitmap(), gameShip.getPosX(), gameShip.getPosY(), paint);

            //add custom typeface
            paint.setTypeface(tf);

            paint.setTextSize(Const.TEXT_TINY * SP);
            paint.setTextAlign(Paint.Align.LEFT);
            canvas.drawText("Lives: " + lives + "   Score: " + score, 10, screenY - 10, paint);
            paint.setTextAlign(Paint.Align.RIGHT);
            canvas.drawText("Ammo: " + ammo + "   Fuel: " + fuel, screenX - 10, screenY - 10, paint);

            /*
            //Developer mode
            paint.setTextAlign(Paint.Align.RIGHT);
            canvas.drawText("Pitch: " + String.format("%.4f", pitch), screenX / 2 - 10, screenY - 10, paint);
            paint.setTextAlign(Paint.Align.LEFT);
            canvas.drawText("Roll: " + String.format("%.4f", roll), screenX / 2 + 10, screenY - 10, paint);
            canvas.drawText("Fps: " + fps, 10, 40, paint);
            */

            if (paused) {
                paint.setTextSize(Const.TEXT_TITLE * SP);
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText("Tartsd kényelmesen a készüléket,",
                        screenX / 2,
                        screenY / 2,
                        paint);
                canvas.drawText("majd érintsd meg a sarokban a play gombot!",
                        screenX / 2,
                        screenY / 2 + ((Const.TEXT_TITLE + 1) * GameView.SP),
                        paint);

                canvas.drawBitmap(pauseBtn.getBitmap(), pauseBtn.getPosX(), pauseBtn.getPosY(), paint);
            } else {
                canvas.drawBitmap(pauseBtn.getBitmap2(), pauseBtn.getPosX(), pauseBtn.getPosY(), paint);
            }



            //Asteroid
            for (int i = 0; i < gameAsteroids.length; i++) {
                if (gameAsteroids[i].getState() != GameAsteroid.DEAD) {
                    if (gameAsteroids[i].getType() == GameAsteroid.NORMAL)
                        canvas.drawBitmap(gameAsteroids[i].getBitmap(), gameAsteroids[i].getPosX(), gameAsteroids[i].getPosY(), paint);
                    else if (gameAsteroids[i].getType() == GameAsteroid.AMMO)
                        canvas.drawBitmap(gameAsteroids[i].getBitmap2(), gameAsteroids[i].getPosX(), gameAsteroids[i].getPosY(), paint);
                    else
                        canvas.drawBitmap(gameAsteroids[i].getBitmap3(), gameAsteroids[i].getPosX(), gameAsteroids[i].getPosY(), paint);
                }
            }

            //Bullet
            for (int i = 0; i < gameBullets.length; i++) {
                if (gameBullets[i].getState() != GameBullet.DEAD) {
                    canvas.drawBitmap(gameBullets[i].getBitmap(), gameBullets[i].getPosX(), gameBullets[i].getPosY(), paint);
                }
            }
            paint.setAlpha(25);
            canvas.drawBitmap(crosshairBtn.getBitmapCross(), crosshairBtn.getPosX(), crosshairBtn.getPosY(), paint);
            //Kirajzol mindent a kijelzore
            surfaceHolder.unlockCanvasAndPost(canvas);

        }
    }


    //Ha a GameActivity elindult, akkor induljon el a thread-ünk is
    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    //Le kell allitani a thread-et, ha a GameActivity szunetel
    public void pause() {
        playing = false;
        paused = true;
        orientationData.pause();
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Hiba", "joining thread");
        }
    }

    //A SurfaceView implementalja az onTouchListenert
    //Szoval ennek a segitsegevel tudjuk detektalni a screen tapit
    //Uzemanyag es loszer tolteset oldjuk meg vele
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & motionEvent.ACTION_MASK) {
            //Touched the screen
            case MotionEvent.ACTION_DOWN:
                if (pauseBtn.getRect().contains(motionEvent.getX(), motionEvent.getY())) {
                    paused = !paused;
                    if (paused) {
                        orientationData.pause();
                    } else {
                        orientationData.register();
                        orientationData.newGame();
                    }
                }

                //uzemanyag es loszer toltese
                if(!paused) {
                    for (int i = 0; i < gameAsteroids.length; i++) {
                        if (gameAsteroids[i].getRect().contains(motionEvent.getX(), motionEvent.getY())) {
                            if (gameAsteroids[i].getType() == GameAsteroid.AMMO) {
                                if(vb != null)
                                    vb.vibrate(200);
                                ammo += 10;
                            } else if (gameAsteroids[i].getType() == GameAsteroid.FUEL) {
                                if(vb != null)
                                    vb.vibrate(200);
                                fuel += 10;
                            }
                        }
                    }
                }

                //Bullet
                //Jobb alsó sarokba nyomva lősz
                if (crosshairBtn.getRect().contains(motionEvent.getX(), motionEvent.getY())) {
                    if (System.currentTimeMillis() - lastBullet > 200 && ammo != 0) {
                        if (ammo > 0)
                            ammo--;
                        gameBullets[bulletCounter % 30].setState(GameBullet.NORMAL);
                        gameBullets[bulletCounter % 30].setPosX(gameShip.getPosX() + gameShip.getWidth() / 2);
                        gameBullets[bulletCounter % 30].setPosY(gameShip.getPosY());
                        bulletCounter++;
                        lastBullet = System.currentTimeMillis();
                    }
                    else if(ammo == 0) {
                        if(vb != null)
                            vb.vibrate(1000);
                    }
                }
                break;
            //Finger removed
            case MotionEvent.ACTION_UP:

                break;
        }
        return true;
    }
}
