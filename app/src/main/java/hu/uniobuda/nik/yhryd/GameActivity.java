package hu.uniobuda.nik.yhryd;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import utility.Const;

//Miutan a Play Game-re mentunk ez az Activity fog futni.
//A GameView metodusait hivja meg, amikor szukseges.
public class GameActivity extends Activity {
    //Ez maga a jatekter. Tartalmazza a megjelenitest, a jatek logikat, es a bemenetekre a valaszt.
    GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Full screen + no titlebar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Ezen objektumon keresztul hozzaferunk az aktualis eszkoz kepernyo tulajdonsagaihoz.
        Display display = getWindowManager().getDefaultDisplay();

        //Egy point objektumba betoltjuk a kijelzo felbontasat.
        Point size = new Point();
        display.getSize(size);

        //gameView inicializalasa + beallitasa view-nak.
        gameView = new GameView(this, size.x, size.y);
        setContentView(gameView);
    }

    @Override
    protected void onResume() {

        super.onResume();
        KeyguardManager myKM = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);
        if(!myKM.inKeyguardRestrictedInputMode()) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            if (sharedPref.getBoolean(Const.KEY_PREF_VOLUME, true)) {
                startService(new Intent(getApplication(), MusicService.class));
            }
        }
        gameView.resume();

    }

    @Override
    protected void onPause() {

        super.onPause();
        gameView.pause();
        if (MusicService.getMusicService() != null)
            MusicService.getMusicService().stopSelf();
    }
}
