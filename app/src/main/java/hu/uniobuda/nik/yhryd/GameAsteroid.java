package hu.uniobuda.nik.yhryd;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;

import utility.Const;
import utility.GameExtent;

import static hu.uniobuda.nik.yhryd.GameView.DP;

/**
 * Created by Nesd on 2017.11.12..
 */

public class GameAsteroid extends GameExtent {
    public static final int AMMO = 5;
    public static final int FUEL = 6;

    private int type;

    public int getType() {
        return type;
    }

    private Bitmap bitmap2;

    public Bitmap getBitmap2() {
        return bitmap2;
    }

    private Bitmap bitmap3;

    public Bitmap getBitmap3() {
        return bitmap3;
    }

    private void setPosY() {
        this.posY = -height;
    }

    private void setPosX(float x) {
        if ((x + width) > screenX)
            this.posX = screenX - width;
        else if (x < 0) {
            this.posX = 0;
        } else
            this.posX = x;
    }


    public GameAsteroid(Context context, int screenX, int screenY, int startX) {
        this.state = DEAD;
        this.type = NORMAL;

        this.screenX = screenX;
        this.screenY = screenY;

        this.width = 40 * DP;
        this.height = 40 * DP;

        if ((startX + width) <= screenX)
            this.posX = startX;
        else
            this.posX = screenX - width;


        this.posY = -height;

        this.speed = screenX / 3;

        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.asteroid1);
        this.bitmap = Bitmap.createScaledBitmap(bitmap, (int) (width), (int) (height), false);

        this.bitmap2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.asteroid2);
        this.bitmap2 = Bitmap.createScaledBitmap(bitmap2, (int) (width), (int) (height), false);

        this.bitmap3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.asteroid3);
        this.bitmap3 = Bitmap.createScaledBitmap(bitmap3, (int) (width), (int) (height), false);

        this.rectF = new RectF();
    }

    public void update(long fps) {
        if (this.state != DEAD) {
            if (posY < screenY + height)
                posY += speed / fps;
            else
                this.state = DEAD;
        }
        // For collision detect
        rectF.left = posX;
        rectF.top = posY;
        rectF.right = posX + width;
        rectF.bottom = posY + height;
    }

    public void Revive(float shipPosX) {
        int rnd = Const.RND.nextInt(100);

        setPosY();
        if (rnd <= 8)
            if (Const.RND.nextInt(2) == 0)
                setPosX(shipPosX - Const.RND.nextInt((int) (20 * DP)));
            else
                setPosX(shipPosX + Const.RND.nextInt((int) (20 * DP)));
        else
            setPosX(Const.RND.nextInt(screenX + 1));

        if (rnd <= 95)
            type = NORMAL;
        else if (rnd <= 97)
            type = AMMO;
        else
            type = FUEL;


        state = NORMAL;
    }
}
