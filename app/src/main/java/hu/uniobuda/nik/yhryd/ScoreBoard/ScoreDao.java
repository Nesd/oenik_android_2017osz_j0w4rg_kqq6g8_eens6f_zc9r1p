package hu.uniobuda.nik.yhryd.ScoreBoard;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by galba on 2017. 12. 09..
 */

@Dao
public interface ScoreDao {

    @Insert
    void insertNewScore(UserScore userScore);

    @Query("SELECT * FROM userscore ORDER BY score DESC LIMIT 10")
    List<UserScore> loadAllScores();
}
