package hu.uniobuda.nik.yhryd;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.WindowManager;


/**
 * Created by Nesd on 2017.11.12..
 * Ez az osztaly szolgaltatja a megfelelo szenzorok adatait, aminek segitsegevel az urhajo mozogni fog.
 */

public class OrientationData implements SensorEventListener {
    private SensorManager manager;

    private Sensor accelerometer;
    private Sensor magnometer;

    private float[] accelOutput;
    private float[] magOutput;

    private float[] orientation = new float[3];
    public float[] getOrientation()
    {
        return orientation;
    }

    private float[] startOrientation = null;
    public float[] getStartOrientation()
    {
        return startOrientation;
    }

    public void newGame()
    {
        startOrientation = null;
    }

    public OrientationData(Context context)
    {
        this.manager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        this.accelerometer = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.magnometer = manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public void register() {
        manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
        manager.registerListener(this, magnometer, SensorManager.SENSOR_DELAY_GAME);
    }

    public void pause() {
        manager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        //Megvizsgaljuk melyik sensorEvent sult el
        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            accelOutput = sensorEvent.values;
        else if(sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            magOutput = sensorEvent.values;

        if(accelOutput != null && magOutput != null)
        {
            float[] R = new float[9];
            //Inclanation Matrix
            float[] I = new float[9];

            boolean success = SensorManager.getRotationMatrix(R, I, accelOutput, magOutput);

            if(success)
            {
                SensorManager.getOrientation(R, orientation);

                if(startOrientation == null)
                {
                    startOrientation = new float[orientation.length];
                    //Referencia tipus, ezert kell az arraycopy
                    System.arraycopy(orientation, 0, startOrientation, 0, orientation.length);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i)
    {

    }
}
