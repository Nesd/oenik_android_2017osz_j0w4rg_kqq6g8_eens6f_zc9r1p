package hu.uniobuda.nik.yhryd;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;

import utility.GameExtent;

import static hu.uniobuda.nik.yhryd.GameView.DP;

/**
 * Created by Nesd on 2017.11.12..
 */

public class GameBullet extends GameExtent {
    public void setPosY(float y) {
        this.posY = y;
    }

    public void setPosX(float x) {
        this.posX = x - width / 2;
    }

    public GameBullet(Context context, int screenX, int screenY) {
        this.state = DEAD;

        this.screenX = screenX;
        this.screenY = screenY;

        this.width = 5 * DP;
        this.height = 20 * DP;

        this.posX = 0;
        this.posY = screenY;

        this.speed = screenX;

        this.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bullet1);
        this.bitmap = Bitmap.createScaledBitmap(bitmap, (int) (width), (int) (height), false);

        this.rectF = new RectF();
    }

    public void update(long fps) {
        if (this.state == NORMAL) {
            if ((posY + height) > 0) {
                posY -= speed / fps;
            } else {
                this.state = DEAD;
            }
        }
        // For collision detect
        rectF.left = posX;
        rectF.top = posY;
        rectF.right = posX + width;
        rectF.bottom = posY + height;
    }

    public boolean isCollided(GameAsteroid[] gameAsteroids) {
        for (int i = 0; i < gameAsteroids.length; i++) {
            if (gameAsteroids[i].getState() != DEAD && this.rectF.intersect(gameAsteroids[i].getRect())) {
                gameAsteroids[i].setState(DEAD);
                this.state = DEAD;
                return true;
            }
        }
        return false;
    }
}
