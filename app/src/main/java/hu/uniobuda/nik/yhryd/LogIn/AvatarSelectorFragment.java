package hu.uniobuda.nik.yhryd.LogIn;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import hu.uniobuda.nik.yhryd.R;
import utility.Const;

/**
 * Created by galba on 2017. 12. 10..
 */

public class AvatarSelectorFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_avatar_select, container, false);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ImageButton button1 = getActivity().findViewById(R.id.imagebutton_avatar1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Const.KEY_PREF_AVATAR, "avatar1");
                editor.apply();

                getActivity().finish();
            }
        });


        ImageButton button2 = getActivity().findViewById(R.id.imagebutton_avatar2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Const.KEY_PREF_AVATAR, "avatar2");
                editor.apply();

                getActivity().finish();
            }
        });

        ImageButton button3 = getActivity().findViewById(R.id.imagebutton_avatar3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Const.KEY_PREF_AVATAR, "avatar3");
                editor.apply();

                getActivity().finish();
            }
        });

        ImageButton button4 = getActivity().findViewById(R.id.imagebutton_avatar4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Const.KEY_PREF_AVATAR, "avatar4");
                editor.apply();

                getActivity().finish();
            }
        });
    }
}
