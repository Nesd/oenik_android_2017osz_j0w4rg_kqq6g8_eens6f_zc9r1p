package hu.uniobuda.nik.yhryd;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by galba on 2017. 11. 15..
 */

public class MusicService extends Service {

    private MediaPlayer mediaPlayer;

    private static MusicService musicService;

    public MusicService(){
        musicService = this;
    }

    public static MusicService getMusicService() {
        return musicService;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        //TODO Rendes zene kell bele
        mediaPlayer = MediaPlayer.create(this, R.raw.bgmusic);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
    }
}
